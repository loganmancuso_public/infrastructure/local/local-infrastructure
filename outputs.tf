##############################################################################
#
# Author: Logan Mancuso
# Created: 07.30.2023
#
##############################################################################

locals {
  split  = split("/", var.network_settings.cidr)[0]
  suffix = "/${split("/", var.network_settings.cidr)[1]}"
  dns    = concat(var.network_settings.dns_servers, [var.network_settings.gateway])
}

output "subnet_lan" {
  value = {
    id      = libvirt_network.lan.id
    name    = var.network_settings.name
    subnet  = local.split
    cidr    = var.network_settings.cidr
    suffix  = local.suffix
    dns     = local.dns
    gateway = var.network_settings.gateway
  }
}