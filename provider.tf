##############################################################################
#
# Author: Logan Mancuso
# Created: 07.30.2023
#
##############################################################################

## Provider ##
terraform {
  required_version = ">= 1.6.0"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = ">=0.7.6"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}
