##############################################################################
#
# Author: Logan Mancuso
# Created: 11.28.2023
#
##############################################################################

resource "libvirt_network" "lan" {
  name      = var.network_settings.name
  mode      = var.network_settings.mode
  domain    = "${var.network_settings.name}.local"
  addresses = [var.network_settings.cidr]
  dns {
    enabled = true
  }
  routes {
    cidr    = var.network_settings.cidr
    gateway = var.network_settings.gateway
  }
  autostart = true
  dhcp {
    enabled = true
  }
}