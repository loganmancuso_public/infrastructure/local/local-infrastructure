##############################################################################
#
# Author: Logan Mancuso
# Created: 07.30.2023
#
##############################################################################

variable "network_settings" {
  description = "Settings to configure the libvirt network"
  type = object({
    name        = string
    mode        = string
    cidr        = string
    gateway     = string
    dns_servers = list(string)
  })
}